import { Company } from './CompanyClass';
import { Candidate } from './CandidateClass';
import { CandidateEvaluator } from './CandidateEvaluatorClass';
import { companiesArr } from './companies-data';


const evaluateAllCompanies = (skills, companyInstances) => {
  let companiesPassed = [];
  
  companyInstances.forEach(company => {
    if ( evaluator.evaluateCandidate(company, candidate) ) {
      companiesPassed.push(company.companyName);
    }
  });

  return companiesPassed
}

const candidate        = new Candidate(['bike', 'driving license']);
const evaluator        = new CandidateEvaluator();
const companyInstances = companiesArr.map(company => {
  return new Company({
    companyName: company.companyName,
    requirements: company.requirements
  })
});


let companiesThatPassed = evaluateAllCompanies(candidate.skills, companyInstances);

let companiesThatDidntPass = companyInstances
  .filter(company => companiesThatPassed.indexOf(company.companyName) === -1)
  .map(company => company.companyName);


console.log(`Companies that I can work for: ${companiesThatPassed}`);
console.log(`Companies that I can't work for: ${companiesThatDidntPass}`);