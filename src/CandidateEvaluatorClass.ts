export class CandidateEvaluator {
  
  // Group companyRequirements in the "AND" statement groups    
  private groupReqByAnd (companyRequirements: string[]): string[][] {
    let reqGroups = [ [] ];

    companyRequirements.forEach(elem => {
      if (elem != 'or' && elem != 'and') {
        reqGroups[reqGroups.length-1].push(elem);
      }
  
      if (elem === 'or') {
        reqGroups.push([]);
      }
    });

    return reqGroups
  }

  //Evaluate "AND" statments group
  private evaluateAndGroup (reqGroups: string[][], candidateSkills: string[]): boolean[] {
    let flatRequirementsArr = [];

    reqGroups.forEach(reqGroup => {
      let groupPassed = true; 

      // For loop, to break early if conditions met
      for (let reqIdx = 0; reqIdx < reqGroup.length; reqIdx++) {
        if (candidateSkills.indexOf(reqGroup[reqIdx]) === -1) {
          groupPassed = false;
          break
        }
      }

      flatRequirementsArr.push(groupPassed);
    });

    return flatRequirementsArr
  }

  // Evaluate "OR" statements group
  private evaluateOrGroup (flatRequirementsArr: boolean[]) : boolean {
    let finalResult = false;
    // For loop, to break early if conditions met
    for (let reqIdx = 0; reqIdx < flatRequirementsArr.length; reqIdx++) {
      if (flatRequirementsArr[reqIdx]) {
        finalResult = true;
        break
      }
    }

    return finalResult
  }

  public evaluateCandidate (company, candidate): boolean {
    let groupedRequirements = this.groupReqByAnd(company.requirements);
    let evaluatedAndGroup = this.evaluateAndGroup(groupedRequirements, candidate.skills);
    return this.evaluateOrGroup(evaluatedAndGroup);
  }
}