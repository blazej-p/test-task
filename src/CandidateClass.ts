export class Candidate {
  skills: string[]

  constructor (skills: string[]) {
    this.skills = skills;
  }
}