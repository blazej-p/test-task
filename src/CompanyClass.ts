
interface ICompany {
  companyName: string
  requirements: string[]
}

export class Company implements ICompany {
  companyName: string
  requirements: string[]

  constructor(company: ICompany) {
    this.companyName  = company.companyName;
    this.requirements = company.requirements;
  }
}

