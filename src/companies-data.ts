// Could be a JSON file, but for the sake of easy import it's a .js file
export const companiesArr = [
  { 
    companyName: 'Company A', 
    requirements: [ 
    'apartment',
    'or',
    'house',
    'and',
    'property insurance'
    ]
  },
  { 
    companyName: 'Company B', 
    requirements: [ 
      '5 door car',
      'or',
      '4 door car',
      'and',
      'driving license'
    ]
  },
  { 
    companyName: 'Company C', 
    requirements: [ 
      'social security number',
      'and',
      'work permit'
    ]
  },
  { 
    companyName: 'Company D', 
    requirements: [
      'apartment',
      'or',
      'flat',
      'or',
      'house' 
    ]
  },
  { 
    companyName: 'Company E', 
    requirements: [ 
      'driving license',
      'and',
      '2 door car',
      'or',
      '3 door car',
      'or',
      '4 door car',
      'or',
      '5 door car'
    ]
  },
  { 
    companyName: 'Company F', 
    requirements: [ 
      'scooter',
      'or',
      'bike',
      'or',
      'motorcycle',
      'and',
      'driving license',
      'and',
      'motorcycle insurance'
    ]
  },
  { 
    companyName: 'Company G', 
    requirements: [ 
      'massage qualification certificate',
      'and',
      'liability insurance'
    ]
  },
  { 
    companyName: 'Company H', 
    requirements: [
      'storage place',
      'or',
      'garage'
    ]
  },
  { 
    companyName: 'Company J', 
    requirements: [
    ]
  },
  { 
    companyName: 'Company K', 
    requirements: [ 
      'paypal account'
    ]
  }
]