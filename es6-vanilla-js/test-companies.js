const skills = [
  'bike',
  'driving license'
]

const companies = [
  { 
    companyName: 'Company A', 
    requirements: [ 
    'apartment',
    'or',
    'house',
    'and',
    'property insurance'
    ]
  },
  { 
    companyName: 'Company B', 
    requirements: [ 
      '5 door car',
      'or',
      '4 door car',
      'and',
      'driving license'
    ]
  },
  { 
    companyName: 'Company C', 
    requirements: [ 
      'social security number',
      'and',
      'work permit'
    ]
  },
  { 
    companyName: 'Company D', 
    requirements: [
      'apartment',
      'or',
      'flat',
      'or',
      'house' 
    ]
  },
  { 
    companyName: 'Company E', 
    requirements: [ 
      'driving license',
      'and',
      '2 door car',
      'or',
      '3 door car',
      'or',
      '4 door car',
      'or',
      '5 door car'
    ]
  },
  { 
    companyName: 'Company F', 
    requirements: [ 
      'scooter',
      'or',
      'bike',
      'or',
      'motorcycle',
      'and',
      'driving license',
      'and',
      'motorcycle insurance'
    ]
  },
  { 
    companyName: 'Company G', 
    requirements: [ 
      'massage qualification certificate',
      'and',
      'liability insurance'
    ]
  },
  { 
    companyName: 'Company H', 
    requirements: [
      'storage place',
      'or',
      'garage'
    ]
  },
  { 
    companyName: 'Company J', 
    requirements: [
    ]
  },
  { 
    companyName: 'Company K', 
    requirements: [ 
      'paypal account'
    ]
  }
]


// Safe evaluation
const evaluateCompany = (skills, companyRequirements) => {

  // Group companyRequirements in the "AND" statement groups
  this.groupReqByAnd = (companyRequirements) => {
    let reqGroups = [ [] ];

    companyRequirements.forEach(elem => {
      if (elem != 'or' && elem != 'and') {
        reqGroups[reqGroups.length-1].push(elem);
      }
  
      if (elem === 'or') {
        reqGroups.push([]);
      }
    });

    return reqGroups
  }


  //Evaluate "AND" statments group
  this.evaluateAndGroup = (reqGroups) => {  
    let flatRequirementsArr = [];

    reqGroups.forEach(reqGroup => {
      let groupPassed = true; 

      // For loop, to break early if conditions met
      for (let reqIdx = 0; reqIdx < reqGroup.length; reqIdx++) {
        if (!skills.includes(reqGroup[reqIdx])) {
          groupPassed = false;
          break
        }
      }

      flatRequirementsArr.push(groupPassed);
    });

    return flatRequirementsArr
  }

  // Evaluate "OR" statements group
  this.evaluateOrGroup = (flatRequirementsArr) => {   
    let finalResult = false;
    // For loop, to break early if conditions met
    for (let reqIdx = 0; reqIdx < flatRequirementsArr.length; reqIdx++) {
      if (flatRequirementsArr[reqIdx]) {
        finalResult = true;
        break
      }
    }

    return finalResult
  }

  let groupedRequirements = this.groupReqByAnd(companyRequirements);
  let evaluatedAndGroup = this.evaluateAndGroup(groupedRequirements);
  return this.evaluateOrGroup(evaluatedAndGroup);
}


const evaluateAllCompanies = (skills, companies) => {
  let companiesPassed = [];
  
  companies.forEach(company => {
    if ( evaluateCompany(skills, company.requirements) ) {
      companiesPassed.push(company.companyName);
    }
  });

  return companiesPassed
}


let companiesThatPassed = evaluateAllCompanies(skills, companies);

let companiesThatDidntPass = companies
  .filter(company => !companiesThatPassed.includes(company.companyName))
  .map(company => company.companyName);


console.log(`Companies that I can work for: ${companiesThatPassed}`);
console.log(`Companies that I can't work for: ${companiesThatDidntPass}`);



